//ES6 UPDATES

/* 1. ARROW FUNCTION EXPRESSION */

/* Bfore ES6, function expression synatx: */

function name(parameters) {
    statements
}

/* function declaration
    name - function name
    param - parameters, placeholder, the name of an argument to be passed to the function
    statements - function body(to be returned) */

//example of normal function
function generateFullName(firstName, middleInitial, lastName) {
    return firstName + ' ' + middleInitial + '. ' + lastName;
}


//in ES6, arrow function expression syntax: 
() >= { //anonymous function name
    statements
}

const variableName = () => {
    statements
}

//example of arrow function expression
const generateFullName = (firstName,middleInitial,lastName) => {
	return firstName + ' ' + middleInitial + '. ' + lastName;
}

//Additional! In arrow function, curly brackets can be omitted but still can return a value
//syntax shorthand:
const add = (x, y) => x + y

//longer version
const add = (x, y) => {
    return x + y;
}

//NOTE: when do we use the arrow function
//1. it can be used when you do not need to name a function 
//2. the function has a simple task to do

//we will use the normal function
let numbers = [1, 2, 3, 4, 5]

let numbersMap = numbers.map(function(number){
    return number * number;
});
console.log(numbersMap);

//in arrow function
let numbers = [1, 2, 3, 4, 5]

let numbersMap = numbers.map((number) => number * number);

console.log(numbersMap);


//2. TEMPLATE LITERALS
//- are string literals allowing embedded expression (concatenation)
const generateFullName = (firstName,middleInitial,lastName) => {
	return firstName + ' ' + middleInitial + '. ' + lastName;
}

//syntax: \n -new line
`string text`
`string text line 1
string text line 2`

`string text ${expression} string text`
//Template Literals can contain placeholders. we used dollar sign and curly brackets. the expressions in the placeholder and the text between the backticks get passed to a function

//concatenation
`${expression}, ${expression}`

console.log(`string text line 1
             string text line 2`)

const generateFullName = (firstName,middleInitial,lastName) => {
    return `${firstName} ${middleInitial}. ${lastName}`;
}

//can also do computations or evaluations inside:
`${8 * 6}`

const add = (x, y) => {
    return x + y;
}

//The sum is

const add = (x, y) => `The sum is ${x + y}`;


//3. DESTRUCTURING ARRAYS AND OBJECTS
//Destructuring assignments syntax is a JS expression that makes it possible to unpack values fro arrays, or properties from objects, into distinct variables.

//normal array
let num = [10, 20];
let num = `${num[0]} ${num[1]}`

//destructuring assignments
let a, b, rest;
[a, b] = [10, 20];

console.log(a);
console.log(b);

[a, b, ...rest] = [10, 20, 30, 40, 50];
console.log(rest);

//example, we have an array that contains a name of a person, if we want to combine it to a full name, we will need to do it like this:
let name = ["Juan", "Dela", "Cruz"]
let fullName = `${name[0]} ${name[1]} ${name[2]}`

//destructuring assignments
//normal object
let firstName, middleName, lastName;

[firstName, middleName, lastName] = ["Juan", "Dela", "Cruz"];

console.log(`${firstName} ${middleName} ${lastName}`)

//let fullName = `${firstName} ${middleName} ${lastName}`
//console.log(fullName);

const person = {
    firstName : "Juan",
    middlename : "Dela",
    lastName : "Cruz"
}

const generateFullName = (firstName, middleName, lastName) => {
    return `${firstName} ${middleName} ${lastName}`
}

generateFullName(person.firstName, person.middleInitial, person.lastName);

//destructuring object
const person = {
    firstName : "Juan",
    middlename : "Dela",
    lastName : "Cruz"
}

const generateFullName = ({firstName, middleName, lastName}) => {
    return `${firstName} ${middleName} ${lastName}`
}

generateFullName(person);



//JSON STRUCTURE
//https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
//https://www.w3schools.com/jsref/dom_obj_event.asp

